const express = require('express');
const { connectToServer } = require("./mongodb");
const {logger} = require('./logger')
const worldCoordinatorController = require('./modulos/worldCoordinator/controller');
const worldEntriesController = require('./modulos/worldEntries/controller');
const worldLinksController = require('./modulos/worldLinks/controller');
const worldOptionsController = require('./modulos/worldOptions/controller');
const worldNewsController = require('./modulos/worldNews/controller');
const worldSearcherController = require('./modulos/worldSearcher/controller');
const {config} = require("./config");

async function runServer(){
  process.on('uncaughtException', err => {
    logger.error({action: 'Fatal error', data:{error:err}});
    setTimeout(() => { process.exit(0) }, 1000).unref() 
  })

  process.on('unhandledRejection', (err, err2) => {
    logger.error({action: 'Fatal error', data:{error:err}});
    setTimeout(() => { process.exit(0) }, 1000).unref() 
  })

  process.on("SIGTERM", (err) => {
    logger.error({action: "Stopping...", data: {error: err}});
    setTimeout(() => { mprocess.exit(1);}, 100).unref();
  });

  try {
    connectToServer();
    logger.info({ event:"MongoDB loaded and connected" });
  } catch (error) {
    logger.error({ event: "MongoDB ERROR: ", error });
    process.exit(1);
  }

  const app = express()

  // Parser de body y cookies
  app.use(express.json());
  app.use(express.urlencoded({ extended: true }));

  app.get("/test", (req, res) => {
    res.header("Content-Type", "application/json");
    res.writeHead(200);
    res.end("Todo ok");
  });

  app.post('/world_entries/options/get/field', (req, res) => { 
    worldOptionsController.getByField(req, res);
  })

  app.post('/world_entries/world/create', (req, res) => { 
    worldCoordinatorController.create(req, res);
  })

  app.post('/world_entries/world/update/news', (req, res) => { 
    worldCoordinatorController.updateNews(req, res);
  })

  app.post('/world_entries/entries/create', (req, res) => { 
    worldEntriesController.create(req, res);
  })

  app.post('/world_entries/links/create', (req, res) => { 
    worldLinksController.create(req, res);
  })

  app.post('/world_entries/options/create', (req, res) => { 
    worldOptionsController.create(req, res);
  })

  app.post('/world_entries/entries/get/all', (req, res) => { 
    worldEntriesController.getAll(req, res);
  })

  app.post('/world_entries/entries/get/class', (req, res) => { 
    worldEntriesController.getReducedByClass(req, res);
  })

  app.post('/world_entries/entries/get/id', (req, res) => { 
    worldEntriesController.getById(req, res);
  })

  app.post('/world_entries/entries/get/reduced/id', (req, res) => { 
    worldEntriesController.getReducedById(req, res);
  })

  app.post('/world_entries/links/get/id', (req, res) => { 
    worldLinksController.getById(req, res);
  })

  app.post('/world_entries/world/get/id', (req, res) => { 
    worldCoordinatorController.getById(req, res);
  })

  app.post('/world_entries/news/update', (req, res) => { 
    worldNewsController.updateNews(req, res);
  })

  app.post('/world_entries/entries/search', (req, res) => { 
    worldSearcherController.search(req, res);
  })

  app.post('/world_entries/world/recent/entries', (req, res) => { 
    worldSearcherController.recentEntries(req, res);
  })

  app.post('/world_entries/world/recent/news', (req, res) => { 
    worldSearcherController.recentNews(req, res);
  })

  app.get('/', (req, res) => {
    res.writeHead(200, 'Content-Type', 'application/json'); 
    res.end('Servicio corriendo ok');
  })

  app.listen(config.port, function () { logger.info({ action: 'Service running on port ' + config.port}) } )
}

runServer();








