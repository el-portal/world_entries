exports.config = {
    mongoUrl: "mongodb://" + process.env.MONGO_SERVICE+ ":" + process.env.MONGO_PORT,
    logDomain: process.env.LOGS_SERVICE,
    logsPort: process.env.LOGS_PORT,
    serviceName: process.env.SERVICE_NAME,
    logLevel: process.env.LOGS_LEVEL,
    port: process.env.WORLD_ENTRIES_PORT,
    urlBase: "http://localhost:5003",
}