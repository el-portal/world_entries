const  MongoClient = require("mongodb").MongoClient;
const {config} = require("./config");

let db;

async function connectToServer() {
    const client = await MongoClient.connect( config.mongoUrl,{ useUnifiedTopology: true, useNewUrlParser: true });
    db =  client.db(config.serviceName);
    if (db) {
      return;
    } else {
      throw new Error ("Fatal error, couldn't connect to MongoDB");
    }
}

function getDb() {
  return db;
}

module.exports = {connectToServer, getDb}
