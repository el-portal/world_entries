// const axios = require("axios");
// const { config } = require("../../config")

// async function createEntry(entryCore, entryLinks){ return new Promise(async (resolve, reject) => {

//     await axios.post( config.urlBase + `/world_entries/entries/create` , {"entryCore":entryCore} ).then( (response) => {
//         if(response.status > 300 ){ console.error("Timeout") }
//     }).catch( (err) => {
//         console.error(err);
//         reject(err);
//     })

//     await axios.post( config.urlBase + `/world_entries/links/create` , {"entryLinks":entryLinks} ).then( (response) => {
//         if(response.status > 300 ){ console.error("Timeout") }
//     }).catch( (err) => {
//         console.error(err);
//         reject(err);
//     })

//     await axios.post( config.urlBase + `/world_entries/options/create` , {"entryCore":entryCore} ).then( (response) => {
//         if(response.status > 300 ){ console.error("Timeout") }
//     }).catch( (err) => {
//         console.error(err);
//         reject(err);
//     })

//     resolve( )
// })}

// async function getById(entryId){ return new Promise(async (resolve, reject) => {

//     let entryCore, entryLinks

//     await axios.post( config.urlBase + `/world_entries/entries/get/id` , {"entryId":entryId} ).then( (response) => {
//         if(response.status > 300 ){ 
//             console.error("Timeout") 
//         } else {
//             entryCore = response.data
//         }
//     }).catch( (err) => {
//         console.error(err);
//         reject(err);
//     })

//     await axios.post( config.urlBase + `/world_entries/links/get/id` , {"entryId":entryId} ).then( (response) => {
//         if(response.status > 300 ){ 
//             console.error("Timeout") 
//         } else {
//             entryLinks = response.data
//         }
//     }).catch( (err) => {
//         console.error(err);
//         reject(err);
//     })

//     resolve( {"entryCore": entryCore, "entryLinks": entryLinks} )
// })}

// module.exports = { createEntry, getById }