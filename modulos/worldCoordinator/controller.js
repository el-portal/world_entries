const { ObjectId, ObjectID } = require("bson");
// const service = require('./service')
const worldEntriesService = require('../worldEntries/service')
const worldLinksService = require('../worldLinks/service')
const worldOptionsService = require('../worldOptions/service')
const worldNewsService = require('../worldNews/service')
const {logger} = require('../../logger')

const fromEntryToCoreLinks = ( entry ) => {

    let entryCore = {}
    let groupedEntryLinks = {}
  
    for (const key of Object.keys(entry)) {
        try {
            if (Array.isArray(entry[key]) && Object.keys(entry[key][0]).includes("_id")) {
                groupedEntryLinks[key] =  entry[key]
            } else {
                entryCore[key] = entry[key]
            }
        } catch {
            logger.debug("Probablemente campo vacío")
        }
    }
  
    entryCore.entryClass = entry.entryClass
    groupedEntryLinks.entryClass = entry.entryClass

    return ( { "entryCore": entryCore, "entryLinks": groupedEntryLinks } )

}

const fromCoreLinksToEntry = ( entryCore, entryLinks ) => {

    const entry = {...entryCore, ...entryLinks}
    return ( entry )

}

async function create(req, res){

    const entry = req.body.entry
    const entryClass = entry.entryClass
    logger.info({ action: `Starting request to create entry ${entry.name} from ${entryClass}`})

    const timestamp = Date.now()
    const entryId = ObjectID()

    let { entryCore, entryLinks } = fromEntryToCoreLinks(entry, entryClass);
    entryCore._id = entryId
    entryLinks._id = entryId
    entryCore.created_ts = timestamp

    // await service.createEntry(entryCore, entryLinks)

    await worldEntriesService.createCore(entryCore)
    await worldLinksService.createLinks(entryLinks)
    await worldOptionsService.createOptions(entryCore)

    res.writeHead(200, 'Content-Type', 'application/json')
    res.end()

}

async function getById(req, res){ return new Promise(async (resolve, reject) => {

    const entryId = req.body.entryId
    logger.info({ action: `Starting request to search for entry by id: ${entryId}`})

    // let { entryCore, entryLinks } = await service.getById(entryId)

    const entryCore = await worldEntriesService.getCoreById(entryId)
    const incompleteEntryLinks = await worldLinksService.getLinksById(entryId)

    delete incompleteEntryLinks._id

    const entryLinks = {}
    try {
        for (const key of Object.keys(incompleteEntryLinks)) {
            for (const incompleteLink of incompleteEntryLinks[key]) {
                    const reducedEntry = await worldEntriesService.getReducedById(incompleteLink._id) 
                    // await axios.post( config.urlBase + `/world_entries/entries/get/reduced/id` , {"entryId":incompleteLink._id} ).then( (response) => {
                    //     if(response.status > 300 ){ 
                    //         console.error("Timeout") 
                    //     } else {
                    //         const reducedEntry = response.data
                    //     }
                    // }).catch( (err) => {
                    //     console.error(err);
                    //     reject(err);
                    // })
                    // resolve(reducedEntry)
                try {
                    entryLinks[key].push(reducedEntry)
                } catch {
                    entryLinks[key] = [reducedEntry]
                }
            }
        }
    } catch {
        console.warn("Probably no links were found")
    }

    if ( entryCore.entryClass === "events" ) {
        try {
            const filledTestimonies = []
            for (const testimony of entryCore.testimonies) {
                const filledTestimony = {...testimony}
                if (testimony.isIndividual) {
                    const reducedEntry = await worldEntriesService.getReducedById(testimony.witnessIndividual[0]._id)
                    filledTestimony.witnessIndividual = [{ ...reducedEntry }]
                }
                filledTestimonies.push(filledTestimony)
            }
            entryCore.testimonies = filledTestimonies
        } catch {
            console.log("Probably no testimonies were found")
        }
    }

    entryLinks._id = ObjectId(entryId)

    const entry = fromCoreLinksToEntry(entryCore, entryLinks)

    res.writeHead(200, 'Content-Type', 'application/json')
    res.end(JSON.stringify(entry))
    resolve(entry)
})}

async function updateNews(req, res){

    const entryId = req.body.entryId
    const newsEntry = req.body.newsEntry
    const newsClass = req.body.newsClass
    logger.info({ action: `Starting request to update event with id ${entryId} with a new ${newsClass}`})

    await worldNewsService.updateNews(entryId, newsEntry, newsClass)

    res.writeHead(200, 'Content-Type', 'application/json')
    res.end()

}

module.exports = { create, getById, updateNews }