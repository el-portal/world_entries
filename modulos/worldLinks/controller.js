const service = require('./service')
const {logger} = require('../../logger')

async function create(req, res){
    try{
        const entryLinks = req.body.entryLinks
        logger.info({ action: `Starting request to create links with entry class ${entryLinks.entryClass}` })
        await service.createLinks(entryLinks)
        logger.info({ action: 'Links created' })
        res.writeHead(201, 'Content-Type', 'application/json')
        res.end()
    } catch {
        logger.info({ action: 'Fail in links creation' })
        res.writeHead(500, 'Content-Type', 'application/json')
        res.end(JSON.stringify({ result:"Fail in links creation"}))
    }
}

async function getById(req, res){

    const entryId = req.body.entryId
    logger.debug({ action: `Starting request to get links related to an entry with id: ${entryId}`})
    
    const entryLinks = await service.getLinksById( entryId )
    
    res.writeHead(200, 'Content-Type', 'application/json')
    res.end(JSON.stringify(entryLinks))
}

module.exports = { create, getById }