const { logger } = require("../../logger");
const { getDb } = require("../../mongodb");
const { ObjectId, ObjectID } = require("bson");

const relatedFields = {
    "events": {
        "happenedIn": "involvedIn",
        "involvedIndividuals": "involvedIn",
        "involvedSocieties": "involvedIn", 
        "involvedObjects":"involvedIn",
    },
    "locations": {
        "includedInLocations": "includesLocations",
        "includesLocations": "includedInLocations",
        "relatedIndividuals": "usualLocations",
        "relatedSocieties": "relatedLocations", 
        "notableTreasure":"possibleLocations",
    },
    "individuals": {
        "usualLocations": "relatedIndividuals",
        "relatedIndividuals": "relatedIndividuals", // usualIndividuals, mejor
        "associatedSocieties": "associatedIndividuals", 
        "notableInventory":"possibleIndividualOwners",
    },
    "societies": {
        "relatedLocations": "relatedSocieties",
        "associatedIndividuals": "associatedSocieties",
        "relatedSocieties": "relatedSocieties", 
        "notableTreasure":"possibleSocietyOwners",
    },
    "objects": {
        "possibleLocations": "notableTreasure",
        "possibleIndividualOwners": "notableInventory",
        "possibleSocietyOwners": "notableTreasure", 
    },
}

const fromEntryLinksToLinks = ( entryLinks ) => {

    const timestamp = Date.now()
    const entryClass = entryLinks.entryClass
    const id = entryLinks._id
    delete entryLinks.entryClass
    delete entryLinks._id

    const allLinks = []
    for (const key of Object.keys(entryLinks)) {
        for (const linkInfo of entryLinks[key]) {
            if (key !== "_id") {
                const link = {
                    _id1: ObjectId(id),
                    _id2: ObjectId(linkInfo._id),
                    field1: key,
                    field2: relatedFields[entryClass][key],
                    entryClass1: entryClass,
                    entryClass2: linkInfo.entryClass,
                    created_ts: timestamp
                }
                allLinks.push( link )
            }
        }
    }

    return ( allLinks )

}

const fromLinksToEntryLinks = ( entryId, allLinks ) => {

    let entryLinks = {}
    if (Array.isArray(allLinks) && typeof allLinks[0] !== "undefined") {
        for (const link of allLinks) {
            let reducedLink, field
            if (entryId == link._id1) {
                field = link.field1
                reducedLink = {
                    _id: link._id2,
                    entryClass: link.entryClass2
                }
            } else if (entryId == link._id2) {
                field = link.field2
                reducedLink = {
                    _id: link._id1,
                    entryClass: link.entryClass1
                }
            } else {
                reducedLink = {}
            }
            try {
                entryLinks[field].push(reducedLink)
            } catch {
                entryLinks[field] = [reducedLink]
            }
        }
    } else {
        logger.warn("No links exist for this entry")
    }
    entryLinks._id = ObjectId(entryId)
    return entryLinks
    
}

async function createLinks(entryLinks){ return new Promise(async (resolve, reject) => {
    logger.info({action: 'Request to create links'})
    allLinks = fromEntryLinksToLinks( entryLinks )
    await getDb().collection('links').insertMany(allLinks).catch( error => {
        console.error(error);
        reject(error);
    })
    resolve();
})}

async function getLinksById( entryId ){ return new Promise(async (resolve, reject) => {
    logger.info({action: `Request to get links for entry with id ${entryId}`})
    await getDb().collection('links').find(
        { $or: [
            {_id1: ObjectId(entryId)}, {_id2: ObjectId(entryId)}
        ] }
    ).toArray(function (err, allLinks) {
        const entryLinks = fromLinksToEntryLinks( entryId, allLinks )
        resolve (entryLinks);
    })
})}

module.exports = { createLinks, getLinksById }