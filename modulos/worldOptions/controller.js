const service = require('./service')
const {logger} = require('../../logger')

async function getByField(req, res){

    const field = req.body.field
    logger.info({ action: `Starting request to get options for field ${field}`})

    const optionsNames = await service.getByField(field)

    res.writeHead(200, 'Content-Type', 'application/json')
    res.end(JSON.stringify(optionsNames))
}

async function create(req, res) {

    const entryCore = req.body.entryCore
    logger.info({action: `Starting request to create new options, if needed, from ${entryCore.name}`})

    await service.createOptions(entryCore)
    res.writeHead(200, 'Content-Type', 'application/json')
}

module.exports = { getByField, create }