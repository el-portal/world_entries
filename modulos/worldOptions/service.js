const { getDb } = require("../../mongodb");
const { logger } = require("../../logger");

const optionsFields = [ "locationType", "race", "occupation", "societyType" ]

async function getByField(field){ return new Promise(async (resolve, reject) => {
    let entryOptionsObjects = await getDb().collection("options").find( { "field":field } ).sort({name: 1}).toArray()
    const optionsNames = entryOptionsObjects.map(optionObject => optionObject.name);
    resolve (optionsNames);
})}

async function createOptions(entryCore){ return new Promise(async (resolve, reject) => {
    logger.info({action: `Request to create new options, if needed, from ${entryCore.name}`})
    let needed = false
    const optionsList = []
    for (const field of Object.keys(entryCore)) {
        if ( optionsFields.includes(field) ) {
            const value = entryCore[field]
            const currentOptions = await getByField(field)
            if ( !currentOptions.includes(value) ) {
                optionsList.push( {"name":value, "field":field} )
                logger.info({action: `Will add a new option: ${entryCore[field]}`})
                needed = true
            }
        }
    }
    if (needed){
        await getDb().collection("options").insertMany(optionsList).catch( error => {
            console.error(error);
            reject(error);
        })
    }
    resolve( ) 
})}

module.exports = { getByField, createOptions }