const service = require('./service')
const {logger} = require('../../logger')

async function create(req, res){

    const entryCore = req.body.entryCore
    await service.createCore(entryCore)

    res.writeHead(200, 'Content-Type', 'application/json')
    res.end()

}

async function getById(req, res){

    const entryId = req.body.entryId
    logger.info({ action: `Starting request to search for entry core by id: ${entryId}`})

    const entry = await service.getCoreById(entryId)

    res.writeHead(200, 'Content-Type', 'application/json')
    res.end(JSON.stringify(entry))
}

async function getReducedById(req, res){

    const entryId = req.body.entryId
    logger.info({ action: `Starting request to search for entry core by id: ${entryId}`})

    const reducedEntry = await service.getReducedById(entryId)

    res.writeHead(200, 'Content-Type', 'application/json')
    res.end(JSON.stringify(reducedEntry))
}

async function getReducedByClass(req, res){

    const entryClass = req.body.entryClass
    logger.info({ action: `Starting request to search for entries of entry class: ${entryClass}`})

    const reducedEntries = await service.getReducedByClass(entryClass)

    res.writeHead(200, 'Content-Type', 'application/json')
    res.end(JSON.stringify(reducedEntries))
}

async function getAll(req, res){

    logger.info({ action: 'Starting request to get all entries'})

    const entries = await service.getAll()
    
    res.writeHead(200, 'Content-Type', 'application/json')
    res.end(JSON.stringify(entries))
}

module.exports = { create, getById, getReducedById, getReducedByClass, getAll }