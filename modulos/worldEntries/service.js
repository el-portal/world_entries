const { ObjectId } = require("bson");
const { getDb } = require("../../mongodb");
const { logger } = require("../../logger")

const reducedEntryProjection = {
    name: 1,
    publicDescription: 1,
    entryClass: 1,
    x: 1,
    y: 1,
    created_ts: 1
}

async function createCore(entryCore){ return new Promise(async (resolve, reject) => {
    logger.info({action: `Request to create core for ${entryCore.name}`})
    await getDb().collection('entries').insertOne(entryCore).catch( error => {
        console.error(error);
        reject(error);
    })
    resolve( )
})}

async function getCoreById( entryId ){ return new Promise(async (resolve, reject) => {
    logger.info({action: `Request to get core for an entry with id ${entryId}`})
    let entryCore = await getDb().collection('entries').findOne({ _id: ObjectId(entryId) })
    resolve( entryCore );
    })
}

async function getReducedById( entryId ){ return new Promise(async (resolve, reject) => {
    logger.info({action: `Request to get reduced entry with id ${entryId}`})
    let [reducedEntry] = await getDb().collection('entries').find(
        { _id: ObjectId(entryId) }
    ).project( reducedEntryProjection ).toArray()
    resolve( reducedEntry );
    })
}

async function getReducedByClass( entryClass ){ return new Promise(async (resolve, reject) => {
    logger.info({action: `Request to get reduced entries with class ${entryClass}`})
    let reducedEntries = await getDb().collection('entries').find(
        { entryClass: entryClass }
    ).project( reducedEntryProjection ).sort({name: 1}).toArray()
    resolve( reducedEntries );
})}

async function getAll(){ return new Promise(async (resolve, reject) => {
    let entries = await getDb().collection('entries').find().toArray()
    resolve( entries );
})}

module.exports = { createCore, getCoreById, getReducedById, getReducedByClass, getAll }