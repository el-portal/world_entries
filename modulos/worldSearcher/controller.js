const service = require('./service')
const {logger} = require('../../logger')

const default_filters = [
    {entryClass: "events"},
    {entryClass: "locations"},
    {entryClass: "individuals"},
    {entryClass: "societies"},
    {entryClass: "objects"},
]
const default_maxNumber_entries = 20
const default_maxNumber_news = 10

async function search(req, res){

    const searchString = req.body.searchString;
    const filters = req.body.filters;
    logger.info({ action: 'Starting request to search by string: ' +  searchString})

    const objectsFound = await service.search( searchString, filters)
    res.writeHead(201, 'Content-Type', 'application/json')
    res.end(JSON.stringify(objectsFound))
}

async function recentEntries(req, res){

    const filters = req.body.filters || default_filters;
    const maxNumber = req.body.maxNumber || default_maxNumber_entries;
    logger.info({ action: `Starting request to search the most recent ${maxNumber} entries`})
    const objectsFound = await service.recentEntries( maxNumber, filters )
    res.writeHead(201, 'Content-Type', 'application/json')
    res.end(JSON.stringify(objectsFound))
    
}

async function recentNews(req, res){

    const maxNumber = req.body.maxNumber || default_maxNumber_news;
    logger.info({ action: `Starting request to search the most recent news from up to ${maxNumber} events`})
    const objectsFound = await service.recentNews( maxNumber )
    res.writeHead(201, 'Content-Type', 'application/json')
    res.end(JSON.stringify(objectsFound))
    
}

module.exports = { search, recentEntries, recentNews }