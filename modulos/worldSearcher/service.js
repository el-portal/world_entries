const { getDb } = require("../../mongodb");
const { logger } = require("../../logger")

const reducedEntryProjection = {
    name: 1,
    publicDescription: 1,
    entryClass: 1,
    x: 1,
    y: 1,
    created_ts:1
}

async function search( searchString, filters ){ return new Promise(async (resolve, reject) => {
    let reducedEntries = await getDb().collection('entries').find(
        { $and: [
            { $text: {$search: searchString} }, 
            { $or: filters }
        ] }
    ).project( {...reducedEntryProjection, score:{ $meta: "textScore"}} ).sort( { score:{ $meta: "textScore"} } ).toArray()
    resolve(reducedEntries)
})}

async function recentEntries( maxNumber, filters ) { return new Promise(async (resolve, reject) => {
    let reducedEntries = await getDb().collection('entries').find(
        {$or: filters}
    ).project(reducedEntryProjection).limit(maxNumber).sort({"created_ts": -1}).toArray()
    resolve(reducedEntries)
})}

async function recentNews( maxNumber ) { return new Promise(async (resolve, reject) => {
    let reducedEntries = await getDb().collection('entries').find(
        { entryClass: "events", diffusion: {$exists: true} }
    ).project(
        { name:1, publicDescription:1, diffusion:1, testimonies:1, created_ts:1, news_ts:1 }
    ).limit(maxNumber).sort({"created_ts": -1}).toArray()
    resolve(reducedEntries)
})}

module.exports = { search, recentEntries, recentNews }