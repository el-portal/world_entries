const { ObjectId } = require("bson");
const { getDb } = require("../../mongodb");
const { logger } = require("../../logger")

async function updateNews(entryId, newsEntry, newsClass){ return new Promise(async (resolve, reject) => {
    logger.info({action: `Request to add ${newsClass} to event with id ${entryId}`})
    
    const timestamp = Date.now()

    if (newsClass === "testimonies") {
        if ( newsEntry.isIndividual ) {
            newsEntry.witnessIndividual = [ { _id: newsEntry.witnessIndividual[0]._id } ]
        }
    }

    await getDb().collection('entries').updateOne(
            {_id: ObjectId(entryId)},
            {$push: { [newsClass]: newsEntry }, $set: {news_ts : timestamp }}
        ).catch( error => {
        console.error(error);
        reject(error);
    })
    resolve( )
})}

module.exports = { updateNews }