const service = require('./service')
const {logger} = require('../../logger')

async function updateNews(req, res){

    const entryId = req.body.entryId
    const newsEntry = req.body.newsEntry
    const newsClass = entry.newsClass
    logger.info({ action: `Starting request to update event with id ${entryId} with a new ${newsClass}`})

    await worldEntriesService.updateNews(entryId, newsEntry, newsClass)

    res.writeHead(200, 'Content-Type', 'application/json')
    res.end()

}

module.exports = { updateNews }